# Prometheus client in Java

Building Docker image:
* mvn clean package dockerfile:build

Prometheus metrics endpoint available at: 
* http://localhost:8081/actuator/prometheus
* Port number configured in *application.properties*.

Other endpoints may be available also (depending on your configuration). The list of available endpoints can be requested:
* http://localhost:8080/actuator/

## References
* https://micrometer.io/
* https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-features.html
* https://www.callicoder.com/spring-boot-actuator/
* https://www.callicoder.com/spring-boot-actuator-metrics-monitoring-dashboard-prometheus-grafana/

