package be.brewcentral.promclientjavademo;

import java.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import reactor.core.publisher.Flux;

@SpringBootApplication
public class PromClientJavaDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PromClientJavaDemoApplication.class, args);
	}

	private static final Logger logger = LoggerFactory.getLogger(PromClientJavaDemoApplication.class);
	private JobExecutionService jobExecutionService;

	public PromClientJavaDemoApplication(JobExecutionService jobExecutionService) {
		this.jobExecutionService = jobExecutionService;
	}

	/**
	 * Simulate job generation.
	 * Every one second, generate a job object and push it to the JobExecutionService.
	 */
	@EventListener(ApplicationReadyEvent.class)
	public void generateJobs() {
		Flux.interval(Duration.ofSeconds(1))
				.map(PromClientJavaDemoApplication::generateJob)
				.doOnEach(r -> jobExecutionService.push(r.get()))
				.subscribe();
	}

	/**
	 * Generate a new job.
	 * @param id job sequence number
	 * @return New job object
	 */
	private static Job generateJob(Long id) {
		// select job type. Choose type1 and type2 with equal probability.
		double r = Math.random();
		String type = r < 0.5 ? "type1" : "type2";
		// duration selection, sampled uniformly between 0.5 and 1.5
		double duration = r + 0.5;
		logger.info("New job generated. Job id: {}. Job type: {}. Duration of execution: {}", id, type, duration);
		return new Job(id, type, duration);
	}



}
