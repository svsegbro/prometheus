package be.brewcentral.promclientjavademo;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Collection;
import java.util.concurrent.ConcurrentLinkedQueue;
import reactor.core.publisher.Flux;

/**
 *
 */
class Job {
    Long jobId;
    String type;
    double duration;

    /**
     * Constructor
     * @param jobId Unique id of the job.
     * @param type Jobs have a type. We will be collecting metrics on the type distribution.
     * @param duration Metrics have a duration for execution. Will be simulated (by sleeping).
     */
    public Job(Long jobId, String type, double duration) {
        this.jobId = jobId;
        this.type = type;
        this.duration = duration;
    }
}

/**
 * An example of a class which generates metrics.
 * Dummy functionality foreseen by this class:
 *   - Holds a job queue to which you can push jobs.
 *   - Continuously executes jobs, if any available. First in, first out.
 */
@Component
public class JobExecutionService {
    private static final Logger logger = LoggerFactory.getLogger(JobExecutionService.class);
    // registry to which metrics will be registered
    private final MeterRegistry meterRegistry;
    // examples of a metric of type "counter"
    private io.micrometer.core.instrument.Counter requestCounter1;
    private io.micrometer.core.instrument.Counter requestCounter2;
    // the job queue
    private ConcurrentLinkedQueue<Job> jobs = new ConcurrentLinkedQueue<>();

    /**
     * Constructor.
     * @param meterRegistry
     */
    public JobExecutionService(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
        // create counters
        requestCounter1 = Counter.builder("jobs")
                .tag("type", "type1")
                .description("Description of counter 1")
                .register(meterRegistry);
        requestCounter2 = Counter.builder("jobs")
                .tag("type", "type2")
                .description("Description of counter 2")
                .register(meterRegistry);
        // create a gauge
        Gauge.builder("jobs.queue", jobs, Collection::size)
                .description("Gauge test")
                .register(meterRegistry);
    }

    /**
     * Push given job to the job queue.
     * @param toAdd Job to be added to the queue
     */
    public void push(Job toAdd) {
        jobs.add(toAdd);
        logger.info("New job in queue. Job id: {}. New queue size: {}.",toAdd.jobId, jobs.size());
        // update counters
        if ("type1".equals(toAdd.type)) {
            requestCounter1.increment(1.0);
        } else if ("type2".equals(toAdd.type)) {
            requestCounter2.increment(1.0);
        }
    }

    /**
     * Execute jobs from queue, one by one.
     */
    @EventListener(ApplicationReadyEvent.class)
    public void run() {
        Flux.interval(Duration.ofSeconds(1))
                .doOnEach(r -> {
                    try {
                        if (!jobs.isEmpty()) {
                            Job jobToBeExecuted = jobs.poll();
                            logger.info("Starting execution of job {}. Duration: {} seconds. New queue size: {}.",
                                    jobToBeExecuted.jobId,
                                    jobToBeExecuted.duration,
                                    jobs.size());
                            Thread.sleep(Math.round(1000 * jobToBeExecuted.duration));
                        }
                    }
                    catch(Exception e) {
                        logger.error("Exception: {}",e.getMessage());
                    }
                })
                .subscribe();
    }
}

