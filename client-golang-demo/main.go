package main

import (
    "flag"
    "github.com/prometheus/client_golang/prometheus"
    "math"
    "math/rand"
    "time"
    "net/http"
    "github.com/prometheus/client_golang/prometheus/promhttp"
)

// Some input parameters
var (
    uniformDomain     = flag.Float64("uniform.domain", 0.0002, "The domain for the uniform distribution.")
    normDomain        = flag.Float64("normal.domain", 0.0002, "The domain (std dev) for the normal distribution.")
    normMean          = flag.Float64("normal.mean", 0.00001, "The mean for the normal distribution.")
    oscillationPeriod = flag.Duration("oscillation-period", 10*time.Minute, "The duration of the rate oscillation period.")
)

// Metrics to be measured and tracked by Prometheus
var (
    // "SummaryVec" is a Collector that bundles a set of Summaries that all share the same Desc, but have different values for their variable labels. 
    // This is used if you want to count the same thing partitioned by various dimensions (e.g. HTTP request latencies, partitioned by status code and method). Create instances with NewSummaryVec.
    // Here we track fictional interservice RPC latencies for three distinct services with different latency distributions.
    // These services are differentiated via a "service" label.
    rpcDurations = prometheus.NewSummaryVec(
        prometheus.SummaryOpts{
            Name:       "client_golang_rpc_duration_sec",
            Help:       "RPC duration distributions.",
            Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
        },
        []string{"service"},
    )

    // Similar to above, but here we track the latencies for one of the three service as a histogram. 
    // The buckets are targeted to the parameters of the normal distribution, with 20 buckets centered on the mean, each half-sigma wide.
    rpcDurationsHistogram = prometheus.NewHistogram(prometheus.HistogramOpts{
        Name:    "client_golang_rpc_duration_hist_sec",
        Help:    "RPC duration distributions.",
        Buckets: prometheus.LinearBuckets(*normMean-5**normDomain, .5**normDomain, 20),
    })

    // "Gauge" is a Metric that represents a single numerical value that can arbitrarily go up and down.
    // A Gauge is typically used for measured values like temperatures or current memory usage, but also "counts" that can go up and down, like the number of running goroutines.
    cpuTemp = prometheus.NewGauge(prometheus.GaugeOpts{
        Name: "client_golang_cpu_temp_celsius",
        Help: "Current temperature of the CPU.",
    })

    // CounterVec is a Collector that bundles a set of Counters that all share the same Desc, but have different values for their variable labels. 
    // This is used if you want to count the same thing partitioned by various dimensions (e.g. number of HTTP requests, partitioned by response code and method). 
    rpcCounts = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "client_golang_rpc_count",
			Help: "Number of requests handled.",
		},
		[]string{"service"},
	)
)

// Register metrics with Prometheus
func init() {
    // Register the summary and the histogram with Prometheus's default registry.
    prometheus.MustRegister(rpcDurations)
    prometheus.MustRegister(rpcDurationsHistogram)
    prometheus.MustRegister(cpuTemp)
    prometheus.MustRegister(rpcCounts)
    // Add Go module build info.
    prometheus.MustRegister(prometheus.NewBuildInfoCollector())
}


func main() {
    flag.Parse()

    start := time.Now()

    // range of the oscillationfactor: [1.2, 2.8]  ( 1.2 = 2 - 0.8 and 2.8 = 2 + 0.8 )
    // the value of *oscillationperiod* affects the times it takes for oscillationFactor to go from the minimal to the maximal value
    // the value of *oscillationFactor* affects the time between any two consecutive metric updates
    oscillationFactor := func() float64 {
        return 2 + math.Sin(math.Sin(2*math.Pi*float64(time.Since(start))/float64(*oscillationPeriod)))
    }

    // Periodical feed of the metric collectors
    go func() {
        for {
            // generate a fictive latency measurement, sampled from a uniform distribution
            duration := rand.Float64() * *uniformDomain
            // have it observed
            rpcDurations.WithLabelValues("uniform").Observe(duration)
    
            // count the incoming request
            rpcCounts.With(prometheus.Labels{"service":"uniform"}).Inc()
            
            // sleep for some time, determined by oscillation factor
            time.Sleep(time.Duration(100*oscillationFactor()) * time.Millisecond)
        }
    }()

    go func() {
        for {
            // generate a fictive latency measurement, sampled from normal distribution with given std deviation and mean
            duration := (rand.NormFloat64() * *normDomain) + *normMean
            // have the measurment observed
            rpcDurations.WithLabelValues("normal").Observe(duration)
            // generate a histogram as well
            rpcDurationsHistogram.Observe(duration)

            // count the incoming request
            rpcCounts.With(prometheus.Labels{"service":"normal"}).Inc()

            // sample temperature, from normal distribution with mean 50 and std dev 10
            cpuTemp.Set((rand.NormFloat64() * 10) + 50)

            // sleep for some time
            time.Sleep(time.Duration(75*oscillationFactor()) * time.Millisecond)
        }
    }()

    go func() {
        for {
            // generate fictive latency measurement, sampled from an exponential distribution with rate parameter lambda equal to 1e6
            duration := rand.ExpFloat64() / 1e6
            // have it observed
            rpcDurations.WithLabelValues("exponential").Observe(duration)
            
             // count the incoming request
            rpcCounts.With(prometheus.Labels{"service":"exponential"}).Inc()
            
            // sleep for some time
            time.Sleep(time.Duration(50*oscillationFactor()) * time.Millisecond)
        }
    }()

    // expose registered metrics
    http.Handle("/metrics", promhttp.Handler())
    http.ListenAndServe(":2112", nil)
}
